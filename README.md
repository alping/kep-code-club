# KEP Code Club

> [**Click here to go to the current Season of the Code Club**](sessions/season-3)

Struggling with your code?
Don't know how to make SAS/STATA/R/Python/whatever program you're using prepare data the way you want it to? Then the KEP code club is for you!

The Code Club is place where we discuss real world programming problems and everyone is welcome to join, regardless of previous programming experience!

## Seasons

- [Season 1](sessions/season-1)
- [Season 2](sessions/season-2)
- [Season 3](sessions/season-3)
