import pandas as pd

###############################################################################
# Snakemake variables
###############################################################################
infile = snakemake.input['infile']
outfile = snakemake.output['outfile']

###############################################################################
# Read and export data
###############################################################################
(pd
    .read_csv(infile, usecols=['id', 'case', 'birth_date'])
    .set_index('id')
    .assign(birth_date = lambda x: pd.to_datetime(x['birth_date']))
    .to_parquet(outfile))
