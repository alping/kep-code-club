import pandas as pd
from numpy import exp, mean
from sklearn.linear_model import LogisticRegression

###############################################################################
# Snakemake variables
###############################################################################
case_control = snakemake.input['case_control']
pneumonia = snakemake.input['pneumonia']
mono = snakemake.input['mono']
table_file = snakemake.output['table']
or_file = snakemake.output['odds_ratios']

###############################################################################
# Read and combine data
###############################################################################
data_raw = pd.read_parquet(case_control, columns=['id', 'case'])

diags_raw = pd.concat(
    [
        pd.read_parquet(pneumonia).rename(columns=lambda x: f'pneumonia_{x}'),
        pd.read_parquet(mono).rename(columns=lambda x: f'mono_{x}')
    ],
    axis='columns')

data = data_raw.join(diags_raw).fillna(0).astype(int)

###############################################################################
# Write result table to file
###############################################################################
(data
    .apply(lambda x: x.value_counts())
    .fillna(0)
    .astype(int)
    .T
    .to_csv(table_file))

###############################################################################
# Calculate odds ratios
###############################################################################
model = LogisticRegression(solver='lbfgs', C=1e8)

model_data = (data > 0).astype(int)

def get_or(data, i):
    compr = data.groupby(['case', f'pneumonia_{i}', f'mono_{i}']).size().reset_index()
    return (model
            .fit(compr[[f'pneumonia_{i}', f'mono_{i}']], compr['case'], compr[0])
            .coef_[0][0])

odds_ratios = exp([get_or(model_data, i) for i in range(4)])

###############################################################################
# Write odds ratios to file
###############################################################################
with open(or_file, 'w') as f:
    f.write(f'mean({", ".join([str(round(o, 4)) for o in odds_ratios])}) = {round(mean(odds_ratios), 4)}')
