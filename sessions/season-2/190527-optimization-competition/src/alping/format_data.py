import pandas as pd

###############################################################################
# Snakemake variables
###############################################################################
case_control = snakemake.input['case_control']
diagnoses = snakemake.input['diagnoses']
outfile = snakemake.output['outfile']

###############################################################################
# Specify age groups
###############################################################################
age_groups = [
    [0, 5],
    [5, 10],
    [10, 15],
    [15, 20]
]

###############################################################################
# Read data
###############################################################################
data_raw = pd.read_parquet(case_control)

diags_raw = pd.read_csv(diagnoses,
    usecols=['id', 'date'], parse_dates=['date'])

###############################################################################
# Exclude all diagnoses after 2012
###############################################################################
diags_limit = diags_raw[diags_raw['date'].dt.year < 2013].copy()

###############################################################################
# Remove repeated diagnoses
###############################################################################
diags_limit = diags_limit.sort_values(['id', 'date'])

diags_limit['days_since_prev'] = (
    diags_limit['date'] -
    diags_limit
        .groupby(['id'])['date']
        .shift(1)
        .fillna(diags_limit['date'] - pd.to_timedelta(100, 'd'))).dt.days

diags_norepeat = diags_limit[diags_limit['days_since_prev'] >= 90]

###############################################################################
# Group into age group
###############################################################################
diags_age = (diags_norepeat
    .join(data_raw['birth_date'], on='id')
    .set_index('id'))

diags_age['age_cat'] = pd.cut(
    (diags_age['date'].dt.year - diags_age['birth_date'].dt.year),
    [a[0] for a in age_groups] + [age_groups[3][1]],
    labels=['0', '1', '2', '3'],
    include_lowest=True)

diags = (pd
    .get_dummies(diags_age['age_cat'])
    .groupby('id').sum()
    .dropna()
    .rename(columns=str))

###############################################################################
# Export data
###############################################################################
diags.to_parquet(outfile)
