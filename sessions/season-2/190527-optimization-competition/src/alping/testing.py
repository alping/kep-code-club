import pandas as pd
import numpy as np
import statsmodels.api as sm
import statsmodels.formula.api as smf


data = pd.read_parquet('data/interim/combined.parquet')
data = (data > 0).astype(int)

test_data = pd.DataFrame(
    [[40, 60, 216, 504], [80, 20, 48, 32]],
    index=pd.Index([1, 0], name='A'),
    columns=pd.MultiIndex.from_arrays([[0, 0, 1, 1], [1, 0, 1, 0]], names=['L', 'Y']))

table = pd.crosstab(data['mono_0'], [data['case'], data['pneumonia_0']])

w = 1 / data.groupby('mono_0')['pneumonia_0'].value_counts(normalize=True)

wtable = (table * w).sum()

(wtable[1, 1] * wtable[0, 1]) / (wtable[1, 0] * wtable[0, 0])
(wtable[1, 1] * wtable[1, 0]) / (wtable[0, 1] * wtable[0, 0])

uwtable = table.sum()
(uwtable[1, 1] * uwtable[0, 1]) / (uwtable[1, 0] * uwtable[0, 0])

p11 = data[data['mono_0'] == 1]['pneumonia_0'].mean()
p10 = data[data['mono_0'] == 0]['pneumonia_0'].mean()
p01 = 1 - p11
p00 = 1 - p10

w11 = (p11 + p10) / p11
w10 = (p11 + p10) / p10
w01 = (p01 + p00) / p01
w00 = (p01 + p00) / p00


p = pd.DataFrame({0: [p00, p01], 1: [p10, p11]})

pd.crosstab(data['mono_0'], data['pneumonia_0']) * (1 / p)

table = pd.crosstab(data['mono_0'], data['pneumonia_0']) * (1 / p)

table.loc[0, 0] * table.loc[1, 1]

compr = data.groupby(['case', 'mono_0', 'pneumonia_0']).size().reset_index()

regfit = sm.GLM(compr['case'], sm.add_constant(compr[['pneumonia_0', 'mono_0']]), freq_weights=compr[0], family=sm.families.Binomial(sm.families.links.logit)).fit()
regfit = smf.logit('case ~ pneumonia_0', data).fit()
regfit = smf.logit('case ~ pneumonia_0 + mono_0', data).fit()
np.exp(regfit.params)

test = table.sum()

data.groupby('mono_0')['pneumonia_0'].value_counts(normalize=True)

pd.crosstab(data['mono_1'], data['pneumonia_1'], normalize=True)

w = 1 / pd.crosstab(data['mono_0'], data['pneumonia_0'], normalize=True)
sw = w * data['pneumonia_0'].value_counts(normalize=True)

wtable = pd.crosstab(data['mono_0'], [data['case'], data['pneumonia_0']])
wtable = wtable * w
wtable = wtable / wtable.sum().sum()
wtable = wtable.sum()

(wtable[1, 1] * wtable[0, 0]) / (wtable[1, 0] * wtable[0, 1])

std = test.sum()

Y1 = (test.loc[0, (1, 1)] + test.loc[1, (1, 1)]) * test.sum(axis='columns')[1]
Y0 = (test.loc[0, (1, 0)] + test.loc[1, (1, 0)]) * test.sum(axis='columns')[1]

(Y1 / (1 - Y1)) / (Y0 / (1 - Y0))

test.sum(axis='columns')


from sklearn.linear_model import LogisticRegression
model = LogisticRegression(solver='lbfgs', C=1e8)

def get_or(data, i):
    compr = data.groupby(['case', f'pneumonia_{i}', f'mono_{i}']).size().reset_index()
    return (model
            .fit(compr[[f'pneumonia_{i}', f'mono_{i}']], compr['case'], compr[0])
            .coef_[0][0])

results = np.exp([get_or(data, i) for i in range(4)])
np.mean(results)





import numpy as np
import pandas as pd
import scipy.stats as stats
import scipy.special as special
import matplotlib.pyplot as plt

from scipy.optimize import minimize

from lifelines import KaplanMeierFitter
kmf = KaplanMeierFitter()

###############################################################################
# Functions
###############################################################################
globalenv = dict(
        normalden=stats.norm.pdf,
        normal=stats.norm.cdf,
        gammaden=stats.gamma.pdf,
        betaden=stats.beta.pdf,
        logit=special.logit,
        invlogit=special.expit,
        exp=np.exp,
        log=np.log
    )

# def negloglike(values, lexpr, data, params):
#     fexpr = lexpr.format(**{param: value for param, value in zip(params, values)})
#     return -data.eval(fexpr, engine='python').sum()

def negloglike(values, lexpr, data, params):
    fexpr = lexpr.format(**{param: value for param, value in zip(params, values)})
    return -np.sum(eval(fexpr, globalenv, data))


def mlexp(lexpr, data, params):
    # negloglike(kwargs.values(), kwargs.keys(), lexpr, globalenv, localenv)

    minimized = minimize(
        fun=negloglike,
        x0=tuple(params.values()),
        args=(lexpr, data, params.keys()),
        method='nelder-mead')

    print(minimized)

    return dict(zip(params, minimized.x))




exp(b0 + b1 * l) / (1 + exp(b0 + b1 * l))
exp(a) / (1 + exp(a))

import numexpr as ne
from scipy.optimize import minimize

y, x, l = data['case'].values, data['pneumonia_0'].values, data['mono_0'].values

a = 5
b0, b1 = 5, 5
c0, c1, c2 = 5, 5, 5

fl = 'l * log(exp(a) / (1 + exp(a))) + (1 - l) * log(1 - exp(a) / (1 + exp(a)))'
fx_l = 'x * log(exp(b0 + b1 * l) / (1 + exp(b0 + b1 * l))) + (1 - x) * log(1 - exp(b0 + b1 * l) / (1 + exp(b0 + b1 * l)))'
fy_xl = 'x * log(exp(c0 + c1 * x + c2 * l) / (1 + exp(c0 + c1 * x + c2 * l))) + (1 - x) * log(1 - exp(c0 + c1 * x + c2 * l) / (1 + exp(c0 + c1 * x + c2 * l)))'

negloglike((1, 2, 3, 4, 5, 6))

def negloglike(p):
    a, b0, b1, c0, c1, c2 = p
    return -ne.evaluate(f'sum({fl} + {fx_l} + {fy_xl})')

minimized = minimize(
    fun=negloglike,
    x0=(0, 0, 0, 0, 0, 0),
    method='nelder-mead')
