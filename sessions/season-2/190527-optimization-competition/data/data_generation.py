import string
import pandas as pd
import numpy as np

from random import shuffle
from itertools import product
from scipy.special import expit, logit
import statsmodels.formula.api as smf

# A mock base dataset with variables included:
#  - ID: A unique id number for all individuals in the dataset
#  - Group: The matching variable, identifying matched cases and controls
#  - Case_cont: 1=case, 2=control
#  - Date_birth= yyyymmdd
# A mock patient registry (MPreg)
# - Primary (hdia) and secondary (bdia) diagnoses
# - Year_diagnosis = Year of diagnosis

rng = np.random.RandomState(0)

###############################################################################
# Specifications
###############################################################################

n_cases = 100000
n_controls = 5
ndia = 22

icd_f = {
    f'icd{n}': (pd
        .read_csv(f'icd{n}_top30.csv')
        .assign(freq=lambda x: x['count'] / x['count'].sum()))
    for n in [8, 9, 10]}

icd_dates = {
    8: ['1964-01-01', '1986-12-31'],
    9: ['1987-01-01', '1996-12-31'],
    10: ['1997-01-01', '2018-12-31']
}

icd_bins = [
    pd.to_datetime(date)
    for date in
    [a[0] for a in icd_dates.values()] + [icd_dates[10][1]]]

age_groups = [
    [0, 5],
    [6, 10],
    [11, 15],
    [16, 20]
]

betas = [
    [-3, 2, 1.1],
    [-3, 2, 1.2],
    [-3, 2, 1.3],
    [-3, 2, 1.4],
]

###############################################################################
# Functions
###############################################################################

def v_randint(low, high):
    low = np.array(low)
    high = np.array(high)

    return low + np.floor(rng.rand(low.shape[0]) * (high - low)).astype(int)


def random_date(min_date, max_date, size=None, padding=(0, 0)):
    min_date = pd.to_datetime(pd.Series(min_date)) + pd.to_timedelta(padding[0], 'd')
    max_date = pd.to_datetime(pd.Series(max_date)) + pd.to_timedelta(padding[1], 'd')

    assert (max_date > min_date).all()

    d = (max_date - min_date).dt.days

    if size:
        min_date = pd.Series(min_date.tolist() * size)
        d = pd.Series(d.tolist() * size)

    days = v_randint(np.zeros(d.size), d)

    return min_date + pd.to_timedelta(days, unit='d')


def get_pneum_code(icd_version):
    if icd_version == 'icd8':
        return str(rng.randint(480, 486 + 1) + round(rng.rand(), 2)).replace('.', ',')
    elif icd_version == 'icd9':
        return str(rng.randint(480, 486 + 1)) + rng.choice(list(string.ascii_uppercase))
    elif icd_version == 'icd10':
        return f'J{rng.randint(120, 189 + 1)}'

def get_mono_code(icd_version):
    if icd_version == 'icd8':
        return f'0{75 + round(rng.rand(), 2)}'.replace('.', ',')
    elif icd_version == 'icd9':
        return '075'
    elif icd_version == 'icd10':
        return f'B27{round(rng.rand(), 1)}'


code_funcs = {
    'pneumonia': get_pneum_code,
    'mono': get_mono_code
}


def fill_out_codes(icd_version, maxdia):
    visit = rng.choice(
        icd_f[icd_version]['hdia'],
        rng.randint(maxdia),
        p=icd_f[icd_version]['freq']).tolist()
    return ' '.join(visit)


def combine_codes(codes):
    code_list = codes.split()
    shuffle(code_list)
    return ' '.join(code_list)


def fill_icd(icds, probs, nrow, ndia, min_date, max_date):
    code_matrix = rng.choice(icds, (nrow, ndia), p=probs)
    return (pd
        .DataFrame(code_matrix, columns=[f'dia{i}' for i in range(ndia)])
        .assign(
            date = random_date(min_date, max_date, nrow),
            id = rng.randint(0, size, nrow)))


def mask_icds(df):
    mask = np.sort(np.c_[
        np.zeros(df.shape[0]),
        rng.binomial(1, 0.20, (df.shape[0], df.shape[1] - 1))])
    return(df.mask(mask.astype(bool)))


def add_codes(variable, group, code_func, ndia):
    codes = data[data[f'{variable}{group}'] == 1]['birth_date'].to_frame()

    codes['variable'] = variable
    codes['group'] = group

    codes['min_date'] = codes['birth_date'] + pd.to_timedelta(age_groups[group][0], 'Y')
    codes['max_date'] = codes['birth_date'] + pd.to_timedelta(age_groups[group][1] + 1, 'Y')

    codes['date'] = random_date(
        codes['min_date'], codes['max_date'], padding=(100,0)).dt.normalize()

    codes['icd_version'] = pd.cut(
        codes['date'],
        icd_bins,
        right=False, labels = ['icd8', 'icd9', 'icd10']).astype(str)

    codes['code'] = codes['icd_version'].apply(code_func)

    codes['fill_codes'] = codes['icd_version'].apply(fill_out_codes, maxdia=ndia)

    codes['all_codes'] = (codes['code'] + ' ' + codes['fill_codes']).apply(combine_codes)

    return codes.join(
        codes['all_codes']
        .str.split(expand=True)
        .rename(columns = lambda x: f'dia{x}'))

###############################################################################
# Create process data
###############################################################################
print('Create population')
# Create cases
cases = pd.DataFrame(dict(
    case = 1,
    group = np.arange(n_cases),
    birth_date = random_date('1964-01-01', '1992-12-31', n_cases)
))

# Create controls matched on cases (how many controls?)
controls = [cases.assign(case = 0) for _ in range(n_controls)]

# Combine cases and controls
data = pd.concat([cases, *controls]).reset_index(drop=True).rename_axis('id')
size = data.shape[0]

# Create mononucleosis
for i, p in enumerate([0.1, 0.2, 0.3, 0.3]):
    data[f'mono{i}'] = rng.binomial(1, p, size)

# Simulate the pneumonia
for i, b in enumerate(betas):
    p = expit(b[0] + b[1] * data[f'mono{i}'] + b[2] * data['case'])
    data[f'pneumonia{i}'] = rng.binomial(1, p)

# Test
# smf.logit('pneumonia0 ~ C(case) + C(mono0)', data).fit().summary()
# [
#     smf.logit(f'case ~ C(pneumonia{i}) + C(mono{i})', data).fit().params[1]
#     for i in range(4)
# ]


###############################################################################
# Create patient register
###############################################################################
# ICD-8: 1964-1968
# ICD-9: 1987-1996
# ICD-10: 1997-2012.

print('Create pneumonia/mono diagnoses')
added_codes = pd.concat([add_codes(variable, group, code_funcs[variable], ndia)
    for variable, group in product(['pneumonia', 'mono'], range(4))])

added_codes = added_codes.reset_index()

print('Repeat some pneumonia/mono diagnoses')
multi_codes = (added_codes
    .assign(days_to_end = (added_codes['max_date'] - added_codes['date']).dt.days)
    .query('days_to_end < 90'))

# multi_codes.query('days_to_end == 0')[['birth_date', 'date', 'max_date', 'days_to_end']]

# Add to dates
multi_codes['date'] = (multi_codes['date'] +
    pd.to_timedelta(
        v_randint(
            multi_codes['days_to_end'] + 20,
            multi_codes['days_to_end'] + 80),
        'd'))

# Recalculate ICD version and code
multi_codes['icd_version'] = pd.cut(
    multi_codes['date'], icd_bins,
    right=False, labels = ['icd8', 'icd9', 'icd10']).astype(str)

multi_codes.loc[multi_codes['variable'] == 'pneumonia', 'code'] = multi_codes.loc[multi_codes['variable'] == 'pneumonia', 'icd_version'].apply(code_funcs['pneumonia'])
multi_codes.loc[multi_codes['variable'] == 'mono', 'code'] = multi_codes.loc[multi_codes['variable'] == 'mono', 'icd_version'].apply(code_funcs['mono'])

# Refill diagnoses
multi_codes = multi_codes.filter(regex='^(?!dia)').copy().drop(columns='days_to_end')

multi_codes['fill_codes'] = (multi_codes['icd_version']
    .astype(str)
    .apply(fill_out_codes, maxdia=ndia))

multi_codes['all_codes'] = (multi_codes['code'] + ' ' + multi_codes['fill_codes']).apply(combine_codes)

multi_codes = multi_codes.join(
    multi_codes['all_codes']
    .str.split(expand=True)
    .rename(columns = lambda x: f'dia{x}'))

# Add back the added diagnoses
print('Combining pneumonia/mono original and repeated')
comb_codes = pd.concat([added_codes, multi_codes]).reset_index(drop=True)

# Number of random ICD observations
nrow = size * 5

print('Generating filler ICDs')
icd8 = fill_icd(icd_f['icd8']['hdia'], icd_f['icd8']['freq'], nrow, ndia, icd_dates[8][0], icd_dates[8][1])
icd9 = fill_icd(icd_f['icd9']['hdia'], icd_f['icd9']['freq'], nrow, ndia, icd_dates[9][0], icd_dates[9][1])
icd10 = fill_icd(icd_f['icd10']['hdia'], icd_f['icd10']['freq'], nrow, ndia, icd_dates[10][0], icd_dates[10][1])

print('Masking filler ICDs')
icd8.loc[:, 'dia0':'dia21'] = mask_icds(icd8.loc[:, 'dia0':'dia21'])
icd9.loc[:, 'dia0':'dia21'] = mask_icds(icd9.loc[:, 'dia0':'dia21'])
icd10.loc[:, 'dia0':'dia21'] = mask_icds(icd10.loc[:, 'dia0':'dia21'])

print('Writing filler ICDs')
filler_icds = pd.concat([icd8, icd9, icd10]).join(data['birth_date'], on='id')
filler_icds = filler_icds.query('date > birth_date')

print('Combining all codes')
all_codes = (pd
    .concat([
        comb_codes[['id', 'date'] + [f'dia{i}' for i in range(ndia)]],
        filler_icds[['id', 'date'] + [f'dia{i}' for i in range(ndia)]]])
    .sort_values(['id', 'date'])
    .reset_index(drop=True))

print('Writing CSV')
data[['case', 'group', 'birth_date']].to_csv('case_control.csv')
all_codes.to_csv('patient_register.csv', index=False)

# Test
# added_codes.head()
# added_codes.groupby('id')['variable'].value_counts().min()

test = comb_codes.query('variable == "pneumonia"')
(test['date'] - test['birth_date']).max() / 365
