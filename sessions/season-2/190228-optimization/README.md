# 190228 - Introduction to Optimization

We welcome you to this semester's first Code Club, with the theme optimization,
i.e. writing more efficient code! If you are uncertain about what optimization
means and/or how to do it in practice, fear not, we will help you figure it out
in this code club session!

## Agenda

1. Brief motivation of why, and maybe more importantly when, optimization should
   be considered
2. Computer anatomy: processors, working memory, and hard drives. How they work,
   their pros and cons, and how you can use this knowledge to write more
   efficient code
3. Practical tips and tricks for writing efficient code (and here we want you to
   chip in with your best dos and don’ts!)
4. Geek out about this code club's exercise problem

## Exercise problem

Try to solve this sessions exercise problem, and then try out a couple of other
ways to do it. Time them (if you know how), and bring them all (even the not so
fast solutions) to the code club so we can discuss what we've learnt!

### Description

Find the number of observation with at least one of the diagnoses 'L651',
'M77\*', 'M1\*\*', in any of the 30 diagnosis columns (the asterisk meaning any
number or letter). In other words, any diagnosis of L651 or any diagnoses
starting with either M77 or just M1.

### Dataset

The data is stored in a compressed CSV file (data/diagnoses.csv.gz). If your
stats tool of choice cannot open compressed files, you can manually extract the
CSV file before using it (on a KEP computer: right click the file -> 7-Zip ->
Extract Here). If you have any issues getting set up, please send an email to
Peter.

These are the first five rows of the dataset:

|  id | dia0 | dia1 | dia2 | ... | dia27 | dia28 | dia29 |
| --- | ---- | ---- | ---- | --- | ----- | ----- | ----- |
|   0 | S240 | O052 | I898 | ... | NA    | NA    | NA    |
|   1 | M801 | M968 | M928 | ... | I888  | NA    | NA    |
|   2 | K270 | M483 | M350 | ... | NA    | NA    | NA    |
|   3 | S146 | Q142 | F400 | ... | NA    | NA    | NA    |
|   4 | H475 | L872 | I631 | ... | NA    | NA    | NA    |

