import requests, io
import numpy as np
import pandas as pd
from scipy.stats import norm
from zipfile import ZipFile

###############################################################################
# Get ICD codes
###############################################################################
def get_codes(file):
    # Decode the file to text
    file_contents = file.read().decode("cp1252")
    # Extract and return codes
    return np.unique([
        t[:4] for t in file_contents.split('\r\n') if t])

# URL to zip archive with ICD codes
icd_url = 'https://www.socialstyrelsen.se/SiteCollectionDocuments/ANSI-2018.zip'

# Download file
r = requests.get(icd_url)

# If download ok, extract files from list and make dataframes
if r.ok:
    with ZipFile(io.BytesIO(r.content)) as z:
        with z.open('KSH97_KOD_20171218.txt') as f:
            icds = get_codes(f)

###############################################################################
# Create data
###############################################################################
ndia = 30
nrow = 1000000

# Set the seed for reproducibility
rng = np.random.RandomState(0)

prob = norm.pdf(np.linspace(-3, 3, icds.shape[0]))
prob = prob / np.sum(prob)

code_matrix = np.ma.masked_where(
    np.sort(np.c_[np.zeros(nrow), rng.binomial(1, 0.20, (nrow, ndia - 1))]),
    rng.choice(icds, (nrow, ndia), p=prob))

# Create dataframe
data = (pd
    .DataFrame(code_matrix, columns=[f'dia{i}' for i in range(ndia)])
    .rename_axis(index='id'))

# data.info()

# Make sure unique, takes a looong time to run
# data.apply(lambda x: pd.Series(x.unique()), axis=1)

# Export to csv
data.to_csv('diagnoses.csv.gz', compression='gzip')
