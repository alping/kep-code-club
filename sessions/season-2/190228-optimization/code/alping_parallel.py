import mmap
import multiprocessing as mp

bcodes = [b'L651', b'M77', b'M1']

def has_dia(code):
    with open('../data/diagnoses.csv', 'r') as f:
        with mmap.mmap(f.fileno(), 0, access=mmap.ACCESS_READ) as mm:
            return {i for i, line in enumerate(iter(mm.readline, b'')) if code in line}


if __name__ == '__main__':
    pool = mp.Pool(processes=4)
    results = pool.map(has_dia, bcodes)

    print(len(results[0] | results[1] | results[2]))
