/*Code club optimization*/

libname code "H:\Code Club\20190208 Optimization";

/*I imported the datafile manually, and will save it now to the lib*/
*data code.club; set import; run; 

/*Taking the dataset from the lib, on running after closing SAS.*/
data club; set code.club; run; 


/*TASK Find the number of observation with at least one of the diagnoses 'L651',
'M77*', 'M1**', in any of the 30 diagnosis columns (the asterisk meaning any
number or letter). In other words, any diagnosis of L651 or any diagnoses
starting with either M77 or just M1.*/

/*For some reason one variable had a different name*/
data club; set club; 
rename var31=dia29; 
run; 

data club2;
set club;
array loop {30} $ dia0--dia29;
do i=1 to 30;
    if loop {i} in: ('L651' 'M77' 'M1' ) then flag=1;
    *if loop {i} in: ('M77') then flag=2; *If want to know num of diagnoses here;
    *if loop {i} in: ('M1') then flag=3; *If want to know num of diagnoses here;
end;
run;

proc freq data=club2; 
table flag; run;

data test; set club2; 
if flag=1; 
run; 


/*Total time to run = 1.49 seconds*/

/*******END ***/
