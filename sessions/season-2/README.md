# KEP Code Club - Season 2

This semester's Code Club sessions will be dedicated to the art of optimization.
Merriam-Webster defines optimization as "an act, process or methodology of making something (such as a design, system or decision) as fully perfect, functional or effective as possible".
When it comes to code, there are numerous different ways of increasing efficiency.
This semester we will talk about optimization in general and also touch on some specific topics.
We would also like to invite you to participate in the Code Clubs first coding challenge!
All meetings are in the Paul Blomqvist room.

## February 28th, 14.00: Introduction to optimization

We will give you an overview of what optimization means when it comes to code and some general tips and tricks to have in mind.
For this meeting, we also want you to bring YOUR best tips, both general and language specific, when it comes to writing efficient code.

## March 21st, 10.00: Dealing with BIG(ish) data

What if the size of my data is bigger than the amount of working memory (RAM) on my computer?
Is there a way to avoid reading the full file into the memory and still do the analysis? Yes, there is!

## April 25th, 14.00: Parallelizing your analyses

Most computers we use today have more than one processor core.
This can be utilized when running big analyses, as we, with a bit of restructuring of the code, can run the calculations in parallel (side-by-side) on multiple cores (think multitasking).
We will talk about what data and methods that are suitable for parallelization and how it can be done in practice.
We will also introduce the coding/optimization competition.

## May 27th, 14.00: Competition

The rules are simple:
the fastest solution on an HP EliteBook LT242 (the newer standard KEP laptop with i7-6500U 2.50GHz, 8GB RAM) running Windows 10 (with WSL) wins.
You can use any programming language (permitted IT will install it), work by yourself or team up with other KEPers.
Speed is the only thing that counts! In order to enter the competition, we need your solution by May 23rd.
Yes, there will be a prize for the winner!

**Hope to see you this spring!** 🌸

Helga, Kelsi, and Peter
