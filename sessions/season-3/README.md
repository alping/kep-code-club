---
title: KEP Code Club
subtitle: Want to become a better coder? Join us for Season 3!
---

# KEP Code Club - Season 3

Curious about other programming languages and statistical software that you usually work with?
Want to learn more about a software you're already using?
Come to one of the Code Club Seminars during 2019/2020 and you'll get the chance to learn more!

Every session first contains an introduction, requiring no previous experience of the software, and then secondly moving into more advanced topics where the presenter will share some of their favorite tips and tricks.
You can bring your laptop and follow along, or just listen in.

If you want to help us make Season 3 of the KEP Code Club the best one yet, please take a few minutes to fill out our anonymous [questionnaire](https://forms.gle/ooQuEZHy2aBwkvjP8).
Your answers will help guide the upcoming sessions to focus on what is most important to you.

**Welcome to Season 3 of the KEP Code Club!** 🌸

Helga, Kelsi and Peter

---

## SAS - November 6th

> Resources: [https://github.com/SCANDAT/SAS](https://github.com/SCANDAT/SAS)

- **Time:** 14:00-15:30
- **Place:** Paul Blomqvist room

**Basic:**
Introduction to SAS focusing on basic concepts

**Advanced:**
Facilitate hanging of ICD-codes including groups and multiple versions (`%icdimport`);
facilitate creating event-time tables, with focus on time-dependent covariates (`%stratify`);
and SAS parallel processing with a method that can be used at KEP and elsewhere.

**Presenters:**
Jing Zhao and Torsten Dahlén are both PhD students at KEP in Gustaf Edgren's study group.
Jing is an intern physician at Karolinska University Hospital and his PhD is focused on studying the epidemiology of blood transfusions.
He's been using SAS for the past 2-3 years, mainly as part of a large data collection process from 20+ different blood banks in Sweden.
Torsten was last year's KEP Code Club Challenge winner and he is a resident physician in Hematology at the Karolinska University Hospital.
He is studying mainly transfusion-transmitted disease utilizing the Scandinavian Donations and Transfusions (SCANDAT) database with focus on agnostic models and multiple testing.

## STATA - November 29th

> Resources: [sessions/season-3/191129-stata](https://gitlab.com/alping/kep-code-club/tree/master/sessions/season-3/191129-stata)

- **Time:** 9:00-10:30
- **Place:** Paul Blomqvist room

**Basic:**
A general introduction to Stata with lots of examples and general programming tips and tricks.

**Advanced:**
Survival analysis in Stata and exporting results to Word, Excel, PDF and $`LaTeX`$.

**Presenter:**
Hannah Bower is research coordinator working in the ARTIS team at KEP with a background in mathematics and statistics.
She previously worked in the development and application of statistical methods for population-based cancer patient survival where Stata was her main programming language.
She has been a Stata user since 2012 and has written commands in Stata which have also utilized Stata's matrix programming language Mata.

## R - December 9th

> Resources: [https://alecri.github.io/courses/R_md_shiny](https://alecri.github.io/courses/R_md_shiny)

- **Time:** 10.00-11.30
- **Place:** Paul Blomqvist room

**Basic:**
An introduction to R with a working example in epidemiology and using (R)Markdown to create documents and reports with inline code and output.

**Advanced:**
How to build a Shiny app with R. Demonstration of a Shiny app to show incidence and mortality of prostate cancer in Sweden.

**Presenter:**
Alessio Crippa is a statistician and R-enthusiast, currently working as a post-doc at the Department of Medical Epidemiology and Biostatistics.
He has over 8 years of experience of working with R and he has written two R packages that are used worldwide.
He also speaks at international useR conferences, is a co-organizer of the Stockholm useR group meetups, and holds seminar for both beginners and experienced R users.
Additional R-related interests include reproducible research, interactive graphics, and Shiny web applications.

## Python - January 15th

> Resources: [https://github.com/mnikolop/PythonTutorial](https://github.com/mnikolop/PythonTutorial)

- **Time:** 15.00-16.30
- **Place:** Paul Blomqvist room

**Basic:**
Description of Python, importing data from different sources, data wrangling, base statistics and data visualization.

**Advanced:**
Advanced usage examples.

**Presenter:**
Markella Nikolopoulou-Themeli (MMSc) is a Health Informatics graduate from KI with experience in data and text mining as well as data engineering and analysis.
After working within healthcare for 2 years, she is now exploring the labor market through the scope of the public sector.

## Version Control and Workflow Management - February 19th

> Presentation slides: [https://alping.gitlab.io/presentations/html/git-snakemake.html](https://alping.gitlab.io/presentations/html/git-snakemake.html)  
> Example project: [https://gitlab.com/alping/git-snakemake-example](https://gitlab.com/alping/git-snakemake-example)

- **Time:** 15:00-16:30
- **Place:** Paul Blomqvist room

**Basic:**
The benefits of using a version control system and a workflow manager, how they work, and *how you could start using them in your own projects today*.
Demonstration of taking a project from start to finish using these tools.
No previous experience/knowledge needed!

**Advanced:**
Remote repositories (push/pull), hosted repositories (GitHub/GitLab), Git tagging and branches (merge/rebase), workflow parallelization.

**Presenter:**
Peter Alping is a PhD student at the Department of Clinical Neuroscience, but spends most of his time at KEP.
His favorite procrastination technique is to find new and interesting tools that could enhance his workflow.
Getting towards the end of his studies, Peter would now like to share the two tools that have improved his way of working the most: a version control system (Git) and a workflow manager (Snakemake).

## Linux - March 16th

- **Time:** 10.30-12:00
- **Place:** Paul Blomqvist room

**Basic:**
General introduction to Linux, how to install programs, basic handling of text files (grep, cat, VIM etc.), and how to remotely work with a server.

**Advanced:**
Using tmux and and an introduction to high performance clusters

**Presenter:**
Helga Westerlind is an assistant professor, co-founder of the KEP code club, and a long term Linux user.
Helga will in this session give an introduction to one of the most powerful operating systems available.
No preparations necessary, everyone welcome!
If you want to follow along during the demonstrations, please install Ubuntu subsystem for Linux on your windows 10 computer (KEP:ers, contact Samir for help!) or install and run Linux from a thumb drive.

## ~~Best Practices in Data Analysis - April 16th~~

> **CANCELED**
