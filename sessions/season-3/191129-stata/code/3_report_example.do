//==============================================================================
// PROGRAM 		report_example
// DATE 		20191122	
// AUTHOR		Hannah Bower
//
// DESCRIPTION	Show different ways to automatically export results from Stata
//
// INPUT		hbp2.dta (webdata)
//
// OUTPUT		table1.doc 
//				logisticmodels.xlsx
//				report_example_docx.docx
//				report_example_pdf.pdf
//
// UPDATED		
//==============================================================================

//=============================================================================
// get data of interest and have a quick look
//=============================================================================
//set up working directory - CHANGE THIS to somewhere where you're happy for files
// to be saved
cd "C:\Users\hanbow\Desktop\KEPcodeclub\Output"

//read in the data & describe dataset
webuse hbp2.dta, clear

//what's in the dataset?
describe

//lets create a cts variable height so we have a cts variable
gen height=rnormal(157, 6) if sex=="female"
replace height=rnormal(176,6) if sex=="male"

//change sex from character to numeric
encode sex, gen(sex2)
drop sex
rename sex2 sex

//can look at distribution of height
histogram height, by(sex) name(histogram_height, replace)

//=============================================================================
// make table 1 using table1_mc
//=============================================================================
//can table1_mc command make life easier? lets install it and see
ssc install table1_mc
table1_mc, by(hbp) vars(city cate \ year cate \ age_grp cate \ sex cate\ height contn) ///
	 saving(table1, replace)

//what is it actually doing?
viewsource table1_mc.ado



//=============================================================================
// now fit logistic regression models
//=============================================================================

// save unadjusted logistic table to excel
putexcel set logisticmodels, sheet(unadjusted model) replace
logistic hbp i.age_grp
putexcel A1= etable


// save adjusted model to another sheet
putexcel set logisticmodels, sheet(fulladjusted model) modify
logistic hbp i.age_grp i.year i.sex i.city i.race height 
putexcel A1= etable

//=============================================================================
// Save only age effect for all models in an excel file - more difficult code
//=============================================================================

 // add covariates one by one- save in locals so we can loop through
 local model1 i.age_grp
 local model2 i.age_grp i.year i.sex
 local model3 `model2' i.city i.race height 
 
 //set up excel sheet with results in
 putexcel set logisticmodels, sheet(resultsforage) modify
 
 //create column for variable
 putexcel A1="Variable", font(bold)
 putexcel A2="20-24 years"
 putexcel A3="25-29 years"
 putexcel A4="30-34 years"

 //save the letters of the alphabet in locals defined by numbers
 tokenize `c(ALPHA)'
 display in green "So the letter A is defined by 1: `1'"

 //loop over models
 forvalues i = 1/3 {
	 //fit models additionally adjusting for variables
	 logistic hbp `model`i''
	 putexcel ``=`i'+1''1 = "OR `i' (95% CI), n=`e(N)' ", font(bold)
	 //loop over the ORs we want to save
	 forvalues j=3/5 {
		// calculate the CIs for saving with the ORs
		local or_lci: di %3.2f = exp(_b[hbp:`j'.age_grp] -1.96* _se[hbp:`j'.age_grp])
		local or_uci: di %3.2f = exp(_b[hbp:`j'.age_grp] +1.96* _se[hbp:`j'.age_grp])
		local or: di %3.2f = exp(_b[hbp:`j'.age_grp])
		//add the results to the 
		putexcel ``=`i'+1''`=`j'-1'="`or' (`or_lci', `or_uci')"
	}
 }
 
 

//=============================================================================
// make pdf report
//=============================================================================

//save histogram of height as a png 
graph export histogram_height.png, replace

//create the pdf 
capture putpdf clear
putpdf begin
putpdf paragraph, font("Calibri",16) halign(center) 
putpdf text ("Report from Stata")
putpdf paragraph, font("Calibri",11) halign(left)
putpdf text ("This report is generated from Stata. We are using some Stata dataset called hbp2.dta and it contains blood pressure measurements from some people. There are ")
count
putpdf text (`r(N)'), bold
putpdf text (" individuals in the dataset. ")

putpdf paragraph, font("Calibri",11) halign(center)
putpdf image histogram_height.png, width(4) 
putpdf save "Report_example_pdf", replace


//=============================================================================
// make word .docx report
//=============================================================================

//do same as above but in word
capture putdocx clear
putdocx begin
putdocx paragraph, font("Calibri",16) halign(center) 
putdocx text ("Report from Stata")
putdocx paragraph, font("Calibri",11) halign(left)
putdocx text ("This report is generated from Stata. We are using some Stata dataset called hbp2.dta and it contains blood pressure measurements from some people. There are ")
count
putdocx text (`r(N)'), bold
putdocx text (" individuals in the dataset. ")

putdocx paragraph, font("Calibri",11) halign(center)
putdocx image histogram_height.png, width(4) 

logistic hbp i.age_grp i.year i.sex i.city i.race height 
putdocx table tablelogistic= etable

putdocx save "Report_example_docx", replace

//=============================================================================
// END OF FILE
//=============================================================================
