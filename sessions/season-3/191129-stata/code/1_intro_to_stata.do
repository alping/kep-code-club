//==============================================================================
// PROGRAM 		intro_to_stata
// DATE 		20191114	
// AUTHOR		Hannah Bower
//
// DESCRIPTION	Quick introduction to Stata for KEP code club 2019
//
// INPUT		brcancer (webdata)
// OUTPUT		NA
//
// UPDATED
//==============================================================================

//can comment like this
*or like this
/*or like this*/

// clear and inport dataset
clear all
webuse brcancer
*webuse brcancer, clear 

// look at the dataset
browse
describe


//different ways to write variable lists
describe x1 x4 x4a x4b
describe x1-x3
describe x*
*describe x


// stata is case sensitive!
*describe X1
describe x1

//rename and drop some variables
rename x1 age
rename x2 menopause
rename x3 tumour_size
keep id hormon age menopause tumour_size x5 rectime censrec
drop x5

//now have a look at dataset
list in 1/3

// look at particular variable
codebook hormon
//oneway and twoway tabulations
tabulate hormon
tabulate hormon menopause 

// summarise a continuous variable
summarize age
summarize age, detail
// look at what this command has saved in the background
return list
//display one of those variables again
display r(p75)

//more ways of summarising
bysort hormon: sum age tumour_size
sum age tumour_size if hormon==1 //logical equals
gen newvar=1 

//boxplot
graph hbox age, over(hormon)
graph hbox age, over(hormon, relabel( 1 "No hormone therapy" 2 "Hormone therapy"))  title("Box plot of age")

// save names
graph hbox age, over(hormon) name(rubbishboxplot, replace)
graph hbox age, over(hormon, relabel( 1 "No hormone therapy" 2 "Hormone therapy")) ///
	title("Box plot of age") name(betterboxplot, replace)


//set tabs to come up in the graph window
set autotabgraphs on, permanently
*set more off, permanently

// could also have labelled the values of the variable and plotted 
label define hormonlabs 0 "No hormone therapy" 1 "Hormone therapy"
label values hormon hormonlabs
tab hormon
graph hbox age, over(hormon) name(boxplot3, replace)


//local and globals & loops
local varlist tumour_size age hormon
foreach var in `varlist' {
	sum `var'
}

global varlist2 tumour_size age hormon
foreach var in $varlist2 {
	sum `var'
}

display "local `varlist'"
display "global $varlist2"



//quick program for correlation of two variables
capture program drop mycorrelate
program mycorrelate
	args var1 var2
	correlate `var1' `var2'
end
mycorrelate age tumour_size

//==============================================================================
// END OF FILE
//==============================================================================

