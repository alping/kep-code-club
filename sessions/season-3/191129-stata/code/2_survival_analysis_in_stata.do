//==============================================================================
// PROGRAM 		survival_analysis_in_stata_simplified
// DATE 		20191122	
// AUTHOR		Hannah Bower
//
// DESCRIPTION	Introduction to survival analysus in Stata for KEP code club 2019
//
// INPUT		diet (webdata)
// OUTPUT		NA
//
// UPDATED		
//==============================================================================

//get the dataset and create an indicator for the event of interest
webuse diet, clear
gen chd=0
replace chd=1 if fail==1 | fail==3 | fail==13

//what is this dataset?
/*data from a pilot study evaluating the use of a weighed diet over 7 days in 
epidemiological studies. The primary hypothesis is the relation between dietary 
energy intake and incidence of coronary heart disease (CHD).*/
describe

//stset: time since entry, chd is event of interest
stset dox, failure(chd=1) origin(doe) enter(doe) scale(365.24)
//what has been created
list id doe dox _t0 _t _d _st _origin

//lets look at KM by bmicat
sts graph, by(hienergy) name(KM_hienergy, replace)

//crude rates in each group
strate
strate hienergy, per(100000)

//lets check on a more informative timescale- attained age
stset dox, failure(chd=1) origin(dob) enter(doe) scale(365.24)
list id doe dob dox _t0 _t _d _st 

//==============================================================================
// compare Poisson, Cox and FPM
//==============================================================================

//POISSON MODEL
//If we wanted to fit a Poisson model, we need to split the timescale, can do this
// using stsplit
stset dox, failure(chd=1) origin(dob) enter(doe) scale(365.24) id(id)
stsplit attage, at(0,50,60,70,100) //every(1)
count
list id _t0 _t _d _st in 1/10

streg i.attage i.hienergy, dist(exp)
//what is saved in the background?
ereturn list
//look at the coefficient matrix
matrix list e(b)
local poisson_coeff: di %5.3f `=exp(_b[_t:1.hienergy])'

//save the model estimates for comparison to other models later
est store poisson



//COX MODEL
//start by reloading data and recreating chd variable
webuse diet, clear
gen chd=0
replace chd=1 if fail==1 | fail==3 | fail==13
tab chd fail

//stset and stcox, save estimates
stset dox, failure(chd=1) origin(dob) enter(doe) scale(365.24)
stcox i.hienergy 
est store cox
local cox_coeff: di %5.3f `=exp(_b[1.hienergy])'



//FLEXIBLE PARAMETRIC MODEL 
stpm2 i.hienergy, scale(h) df(5)
est store fpm
local fpm_coeff: di %5.3f `=exp(_b[xb:1.hienergy])'



//compare model estimates
estimates table poisson cox fpm
di in red "Coefficient estimates from 1) Poisson= `poisson_coeff', 2) Cox=`cox_coeff' and, 3) FPM=`fpm_coeff'"



//==============================================================================
// Further look at flexible parametric survival models- checking degrees of freedom
//==============================================================================

//the user has to choose the degrees of freedom in FPMs, let's loop over them 
// and compare predicted hazard rates and AIC/BIC values

forvalues df=2/6 {
	//fit model and save estimates
	qui stpm2 i.hienergy, scale(h) df(`df')
	est store model_df`df'
	
	//predict the hazard ratio 
	cap drop haz_df`df'*
	predict haz_df`df', hazard at(hienergy 1) ci

}

//look at what has been created in the dataset
describe

//now plot the hazard rates
forvalues df=2/6 {
	local linelist `linelist' (line haz_df`df' _t, sort lpattern(solid))
}
graph twoway `linelist', name(hazards_diff_df, replace) ///
		title("Hazard rates with different df for high energy group") ///
		xtitle("Attained age") ///
		ytitle("Hazard rate") ylabel(0 (0.005) 0.015 ,angle(h)) ///
		legend(ring(0) pos(11) cols(1) order(1 "2df" 2 "3df" 3 "4df" 4 "5df" 5 "6df")) 
		
		
//look at the AIC/BIC to select best degrees of freedom
estimates stat model_df*		


//predict for user-defined timescale- useful to make things faster when you have
// a large dataset
cap drop temptime haz*
range temptime 30 50 200
predict haz, h timevar(temptime) at(hienergy 1) ci
line haz temptime, sort name(haz_with_temptime, replace)



//==============================================================================
// Further look at flexible parametric survival models- time-dependent effects (non ph models)
//==============================================================================

//time-dependent effects model- let's fit ph and nonph model and see if we need
// nonph via likelihood ratio test
stpm2 i.hienergy, scale(h) df(2) 
est store phmodel

stpm2 i.hienergy, scale(h) df(2) tvc(hienergy) dftvc(2)
est store nonphmodel
lrtest phmodel  nonphmodel //don't actually have non-ph but let's pretend we do

//predict hazards from nonph model
predict h_hi, h at(hienergy 1)
predict h_lo, h at(hienergy 0)

graph twoway (line h_hi _t, sort lcolor(black)) ///
		(line h_lo _t, sort lcolor(red)) ///
		, name(nonph, replace) ///
		title("Hazard rates from nonPH model") ///
		xtitle("Attained age") ///
		ytitle("Hazard rate") ylabel(0 (0.005) 0.015 ,angle(h)) ///
		legend(ring(0) pos(11) cols(1) order(1 "High energy" 2 "Low energy" )) 


//==============================================================================
// END OF FILE
//==============================================================================



