
//==============================================================================
// PROGRAM 		stata_to_latex_table_example.do
// DATE 		20161121	
// AUTHOR		Hannah Bower
//
// DESCRIPTION	Show how to export results to LaTeX
//
// INPUT		diet.dta (webdata)
//
// OUTPUT		stata_to_latex_table.tex
//
// UPDATED		20191125 
//==============================================================================

// define current directory- CHANGE THIS!!
clear
cd C:\Users\hanbow\Desktop\KEPcodeclub\Output

// use diet data from Stata's website
webuse diet

// we need to save the values we're interested in in locals in order to put them 
// in a table. We'll split them by the hienergy variable 
tab hienergy

//look at other variables
codebook job height

// save values for categorical variables by hienergy
//low energy
tab hienergy job
foreach i in 0 1 2 {
	count if job==`i' & hienergy==0
	local job`i'_low `r(N)'
}
//high energy
foreach i in 0 1 2 {
	count if job==`i' & hienergy==1
	local job`i'_high `r(N)'
}


// continuous variable
//height, low energy
sum height if hienergy==0
local meanheight_low: di %3.1f  `r(mean)'
local sdheight_low: di %4.3f  `r(sd)'

// height, high energy
sum height if hienergy==1
local meanheight_high: di %3.1f `r(mean)' 
local sdheight_high: di %4.3f `r(sd)'

// define the file we want to save the table in (called table saved at stata_to_latex.tex)
cap file close table
file open table using stata_to_latex_table.tex, write replace

//start writing table using LaTeX code
file write table ///
"\begin{table}[ht]" _newline ///
"\centering" _newline ///
"\caption{Example Table 1 from the diet data}" _newline ///
"\label{Table}" _newline ///
"\begin{tabular}{l| c c}" _newline ///
"\hline" _newline ///
" &\bf Low energy & \bf High energy \\" _newline ///
"\hline" _newline /// 
"{\bf Height}, mean (SD) & `meanheight_low' (`sdheight_low') & `meanheight_high' (`sdheight_high') \\" _newline ///
"{\bf Job} (N) \\" _newline ///
"\hspace{0.1in} 0 & `job0_low' & `job0_high' \\" _newline ///
"\hspace{0.1in} 1 & `job1_low' & `job1_high' \\" _newline ///
"\hspace{0.1in} 2 & `job2_low' & `job2_high' \\" _newline ///
"\hline" _newline /// 
"\end{tabular}%" _newline ///
"\end{table}" _newline	
file close table


//=============================================================================
// END OF FILE
//=============================================================================
