* Master ICD-10 file
* (perhaps doing this is unnecessary but it can be quite useful to have)


***************************************************************************************************************************************************************************************
preserve
insheet using "H:\KI seminars\KEP coding club\ICD-10\sessions_181108-icd-codes_data_icd_codes.csv", clear names
gen len=strlen(code)
* max length 5 (max 5 characters in the code)
gen icd_l=substr(code, 1,1)
gen icd_num=substr(code, 2,2)
gen icd_num_1=substr(code,4,1)
gen icd_num_1l=substr(code,5,1)
rename code code_code
rename desc desc_code
save "H:\KI seminars\KEP coding club\ICD-10\code.dta", replace

insheet using "H:\KI seminars\KEP coding club\ICD-10\sessions_181108-icd-codes_data_icd_categories.csv", clear names
gen len=strlen(code)
* max 3 characters in the code
drop len
gen icd_l=substr(code, 1,1)
gen icd_num=substr(code, 2,2)
rename desc desc_cat
rename code code_cat
save "H:\KI seminars\KEP coding club\ICD-10\categories.dta", replace

* note: no letters V, W, X, Y in codes file
merge 1:m icd_l icd_num using "H:\KI seminars\KEP coding club\ICD-10\code.dta"
rename _m m1
save "H:\KI seminars\KEP coding club\ICD-10\code_cat.dta", replace

insheet using "H:\KI seminars\KEP coding club\ICD-10\sessions_181108-icd-codes_data_icd_subchapters.csv", clear names
gen start=substr(code, 1, 3)
gen start_l=substr(start, 1,1)
gen start_num=substr(start, 2, 2)
gen end=substr(code, 5, 3)
gen end_l=substr(end, 1,1)
gen end_num=substr(end, 2, 2)

gen ind=1 if start_l==end_l
replace ind=0 if ind==.
tab code if ind==0
* X85-Y09 - the only subchapter containing combination of different letters

destring end_num, gen(end_num1) force
destring start_num, gen(start_num1) force
gen exp=end_num1-start_num1 if ind==1
rename code code_sub
rename desc desc_sub
save "H:\KI seminars\KEP coding club\ICD-10\subchapter.dta", replace

******************************************************************
* quick check
******************************************************************
use "H:\KI seminars\KEP coding club\ICD-10\code_cat.dta", clear
destring icd_num, force replace
table icd_l, c(min icd_num max icd_num)
save "H:\KI seminars\KEP coding club\ICD-10\code_cat.dta", replace 
/*------------------------------------
    icd_l | min(icd_num)  max(icd_num)
----------+---------------------------
        A |            0            99
        B |            0            99
        C |            0            97
        D |            0            89
        E |            0            90
        F |            0            99
        G |            0            99
        H |            0            95
        I |            0            99
        J |            0            99
        K |            0            93
        L |            0            99
        M |            0            99
        N |            0            99
        O |            0            99
        P |            0            96
        Q |            0            99
        R |            0            99
        S |            0            99
        T |            0            98
        V |            1            99
        W |            0            99
        X |            0            99
        Y |            0            98
        Z |            0            99
--------------------------------------*/
******************************************************************

use "H:\KI seminars\KEP coding club\ICD-10\subchapter.dta", clear
* X ranges from 0-99
count
*261 obs
expand 2 if ind==0
gen icd_l=start_l
bysort ind: replace icd_l=end_l if ind==0 & _n==2
replace exp=9 if ind==0 & icd_l=="Y"
display 99-85
replace exp=14 if ind==0 & icd_l=="X"
replace exp=exp+1

expand exp
* max exp=30
gen icd_num=start_num
destring icd_num, force replace
forvalues x=2/31 {
local x1=`x'-1
bysort code: replace icd_num=icd_num[1]+`x1' if _n==`x'
}
save "H:\KI seminars\KEP coding club\ICD-10\subchapter_exp.dta", replace

merge m:m icd_l icd_num using "H:\KI seminars\KEP coding club\ICD-10\code_cat.dta"
rename _m m2
save "H:\KI seminars\KEP coding club\ICD-10\code_cat_sub.dta" 

insheet using "H:\KI seminars\KEP coding club\ICD-10\sessions_181108-icd-codes_data_icd_chapters.csv", clear names
gen start=substr(code, 1, 3)
gen start_l=substr(start, 1,1)
gen start_num=substr(start, 2, 2)
gen end=substr(code, 5, 3)
gen end_l=substr(end, 1,1)
gen end_num=substr(end, 2, 2)

gen ind=1 if start_l==end_l
replace ind=0 if ind==.
tab code if ind==0
* 4 chapters contain codes starting with different letters
destring end_num, gen(end_num1) force
destring start_num, gen(start_num1) force
gen exp=end_num1-start_num1 if ind==1

expand 2 if ind==0 & code!="V01-Y98"
expand 4 if ind==0 & code=="V01-Y98"

gen icd_l=start_l
bysort ind icd_l: replace icd_l=end_l if ind==0 & _n==2 & code=="A00-B99"
bysort ind icd_l: replace icd_l=end_l if ind==0 & _n==2 & code=="C00-D48"
bysort ind icd_l: replace icd_l=end_l if ind==0 & _n==2 & code=="S00-T98"

bysort ind icd_l: replace icd_l="W" if ind==0 & _n==2 & code=="V01-Y98"
bysort ind icd_l: replace icd_l="X" if ind==0 & _n==3 & code=="V01-Y98"
bysort ind icd_l: replace icd_l="Y" if ind==0 & _n==4 & code=="V01-Y98"

replace exp=99 if ind==0 & icd_l=="A"
replace exp=99 if ind==0 & icd_l=="B"
replace exp=97 if ind==0 & icd_l=="C"
replace exp=48 if ind==0 & icd_l=="D"
replace exp=99 if ind==0 & icd_l=="S"
replace exp=98 if ind==0 & icd_l=="T"
replace exp=99 if ind==0 & icd_l=="V"
replace exp=99 if ind==0 & icd_l=="W"
replace exp=99 if ind==0 & icd_l=="X"
replace exp=98 if ind==0 & icd_l=="Y"
replace start_num="00" if ind==0 & icd_l=="W"
replace start_num1=0 if ind==0 & icd_l=="W"
replace start_num="00" if ind==0 & icd_l=="X"
replace start_num1=0 if ind==0 & icd_l=="X"
replace start_num="00" if ind==0 & icd_l=="Y"
replace start_num1=0 if ind==0 & icd_l=="Y"

replace exp=exp+1
expand exp

rename code code_ch
rename desc desc_ch
save "H:\KI seminars\KEP coding club\ICD-10\chapter_exp.dta", replace

gen icd_num=start_num
destring icd_num, force replace
forvalues x=2/100 {
local x1=`x'-1
bysort code icd_l: replace icd_num=icd_num[1]+`x1' if _n==`x'
}

merge m:m icd_l icd_num using "H:\KI seminars\KEP coding club\ICD-10\code_cat_sub.dta"
save "H:\KI seminars\KEP coding club\ICD-10\code_cat_sub_ch.dta", replace
***************************************************************************************************************************************************************************************

*****************
* Tasks
*****************

insheet using "H:\KI seminars\KEP coding club\ICD-10\sessions_181108-icd-codes_data_visits.csv", clear
save "H:\KI seminars\KEP coding club\ICD-10\data.dta"

gen icd_l=substr(main_dia, 1,1)
* 11 missing
* 16 obs start with a number (1-8) instead of the letter
gen icd_num1=substr(main_dia, 2,2)
* all string valuess to missing, keep only numbers
destring icd_num1, gen(icd_num2) force
* 88 missing
* 1 obs with only single number 0, 7 and 8, replace to missing
replace icd_num2=. if icd_num1=="8" | icd_num1=="7" | icd_num1=="0"
gen icd_num_s=icd_num1 if icd_num2!=.
destring icd_num_s, gen(icd_num) force

* 1. How many visits have a main diagnosis from the category "M05"?
count if icd_l=="M" & icd_num_s=="05"
* 175

* 2. How many visits have a main diagnosis from chapter 10 (J00–J99)?
count if icd_l=="J" & inrange(icd_num, 0, 99)
* 18

gen code= icd_l+icd_num_s if icd_num_s!=""

* 3. How many patients have at least one main diagnosis from chapter 10 (J00–J99)?
* any visit from chapter 10
* not really what was asked for, but done anyhow
* Split diagnoses in separate variables containing strings from var diagnosis separted by space
split diagnoses, gen(dg)
* for each of separate diagnosis variables (dg1-dg4) generate chapter letter indicator if the first character of the var is a letter, and a number indicator if the first two characters are numbers
foreach var of varlist dg* {
gen `var'_l=substr(`var', 1, 1)
replace `var'_l="" if inrange(`var'_l, "1", "9")
gen `var'_num=substr(`var', 2, 2)
destring `var'_num, gen(`var'_num_new) force
replace `var'_num_new=. if `var'_l==""
}

foreach var of varlist dg1 dg2 dg3 dg4 {
count if `var'_l=="J" & inrange(`var'_num_new, 0, 99)
}
* dg1=18
* dg2=15
* dg3=7
* dg4=3

* or to see who has at least one diagnosis from chapter 10
gen chap10=1 if icd_l=="J" & inrange(icd_num, 0, 99)
foreach var of varlist dg1 dg2 dg3 dg4 {
replace chap10=1 if `var'_l=="J" & inrange(`var'_num_new, 0, 99)
}
* 42 visits have at least 1 diagnosis from chapter 10

* The actual question
* per patient
gen chap10p=1 if icd_l=="J" & inrange(icd_num, 0, 99)
bysort patient_id chap10p: replace chap10p=. if _n>1
tab chap10p
* 17 patients
save "C:\Users\renzel\Desktop\code club\example.dta", replace


* 4. Within visits, which sub-chapters have a frequency of 1% or more?
gen id=_n
merge m:m icd_l icd_num using "C:\Users\renzel\Desktop\code club\icd10.dta"
bysort id: keep if _n==1
tab code_sub
* diagnoses marked with 1% are % out of everybody, including also the erroneous codes
*  C81-C96 * (1%)
*  D37-D48 * (1%)
*  E70-E90 *
*  G40-G47 *
*  I30-I52 *
*  L20-L30 * (1%)
*  L60-L75 *
*  L80-L99 *
*  M05-M14 *
*  Q80-Q89 *
*  T36-T50 * (1%)
*  T80-T88 *
*  Z00-Z13 *
*  Z80-Z99 *

* Within patients, which sub-chapters have a frequency of 1% or more?
drop if patient_id==.
bysort patient_id: gen pnum=1 if _n==1
* 291 unique patients
* actually, not really sure what i was supposed to do here - not done!


* 6. How many patients have a code for "Multiple Sclerosis"?
* G35
count if icd_l=="G" & icd_num==35
* 4 visits
gen ms=1 if icd_l=="G" & icd_num==35
bysort patient_id ms: replace ms=. if _n>1
* 4 patients

* 7. Are there any invalid codes in the dataset? If so, how many?
* 3 characters - 91 missing
gen four=substr(main_dia, 4,1)
destring four, force replace
count if icd_l=="" | icd_num==. | four==.
* 4 characters - 93 missing
