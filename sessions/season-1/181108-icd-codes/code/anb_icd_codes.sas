/******************************************************
Author: Andrei Barbulescu
Date: 25-OCT-2018
Project: Coding Club - ICD stuff

Description of the program (what it does): solves the problems proposed in 'coding club 2 - ICD10 codes'
Updates:

Input (data-sets it starts with): D:\Andrei\KEP PhD\Seminars & JC\Coding_Club\8 NOV\DATA\ICD_visits.csv
Output (data sets it produces): various
*********************************************************/


/*	load the data	*/  

DATA visits;
INFILE 'D:\Andrei\KEP PhD\Seminars & JC\Coding_Club\8 NOV\DATA\ICD_visits.csv' DLM = ',' DSD MISSOVER FIRSTOBS=2;
INPUT patient_id :3. visit_date :YYMMDD10. onset_date ::YYMMDD10. main_dia :$4. diagnoses :$30.;
FORMAT visit_date MMDDYY10. onset_date MMDDYY10.;
RUN;

**	check length of diagnoses
	to see if the attributed value of '$30.' was enough ;

DATA visits;
SET visits;
diag_len=length(diagnoses);
RUN;

PROC MEANS DATA=visits MIN MAX;
VAR diag_len;
RUN;

**	max length is 23 characters.
	the maximum of 30 was not attained. OK; 

/* extract the ICD codes in "diagnoses" as separate variables */

%macro extr_diags;
DATA visits_2;
SET visits;
%DO i=1 %TO 4;
	dia_&i.=SCAN(diagnoses, &i., ' ');
%END;
RUN;
%mend extr_diags;

%extr_diags;

/*	sort by patient_id	*/
PROC SORT DATA=visits_2;
BY patient_id;
RUN;
*	# there are several visits per patient ;


/* Exercises */

/* 1, 2 */
*1*	How many visits have the diagnosis M05
	(Seropositive rheumatoid arthritis)
*2*	How many visits have a main diagnosis from chapter 10 (J00�J99)?
	! visits are observations !
;
DATA visits_3;
SET visits_2;
i_spra=0;
i_resp=0;
IF main_dia IN: ('M05') THEN i_spra=1;
IF main_dia IN: ('J') THEN i_resp=1;
RUN;

PROC FREQ DATA=visits_3;
TABLES i_spra i_resp;
RUN;

**	ANSWER: there were:
				# 175 visits with main diagnosis 'M05'
				# 20 visits with main diagnosis 'J...'
;


/* 3 */
*	How many patients have at least one main diagnosis from chapter 10 (J00�J99)? ;

**	I have already marked the visits that contain a main diagnosis from chapter 10;
**	take a look at how many patients had at lest one visit;
**	this can be done by sorting the observations descending for i_resp,
	and keeping only one observation per patient (the first one is kept);

PROC SORT DATA=visits_3;
BY patient_id DESCENDING i_resp;
RUN;

*	keep one line per patient;
PROC SORT DATA=visits_3 OUT=visits_4 NODUPKEY;
BY patient_id;
RUN;

PROC FREQ DATA=visits_4;
TABLES i_resp;
RUN;

**	ANSWER:
	19 patients had at least one visit with a diagnostic from "J".

	since there were 20 visits with a diagnosis for "J" in total,
	it means that there was one patients who had 2 visits with "J",
	while the other visits were in different patients;

/* 4 */
*	Within visits, which sub-chapters have a frequency of 1% or more? ;

**	For this I need to label each visit according to the subchapter
	that the main diagnosis belongs to;
**	Subchapters are defined in a file;
DATA subchapters;
INFILE '"D:\Andrei\KEP PhD\Seminars & JC\Coding_Club\8 NOV\DATA\ICD-codes_data_icd_subchapters.csv"' DLM = ',' DSD MISSOVER FIRSTOBS=2;
INPUT code $ desc $;
RUN;

DATA subchapters_2;
SET subchapters;
DROP desc;
chap=SUBSTR(code,1,1);
ch_min=SCAN(code, 1, '-');
ch_max=SCAN(code, -1, '-');
RUN;

**	create chapter and visit_id variable in the visits data-set as well ;
DATA visits_5;
SET visits_2;
chap=SUBSTR(main_dia,1,1);
vis_id+1;
RUN;

PROC SORT DATA=visits_5;
BY chap;
RUN;

**	merge to each visit all lines for the corresponding chapter ;
PROC SQL;
CREATE TABLE visits_6
AS SELECT
	a.patient_id,
	a.vis_id,
	a.main_dia,
	a.chap AS chap_a,
	b.chap AS chap_b,
	b.ch_min,
	b.ch_max,
	b.code AS subchap
FROM
	visits_5 AS a
	LEFT JOIN
	subchapters_2 AS b
ON a.chap = b.chap;
QUIT;

PROC SORT DATA=visits_6;
BY chap_a patient_id vis_id;
RUN;

**	create an indicator that is 1
	when the ICD code matches the subcahpter;
DATA visits_6;
SET visits_6;
i_subch=0;
IF ch_min<=SUBSTR(main_dia,1,3)<=ch_max THEN i_subch=1;
IF MISSING(main_dia)=1 THEN i_subch=0; /* there are some visits with no main_dia */
RUN;

**	keep only one line per visit.
	the line that has i_subch=1 (if any such line) - that's the matching subchapter label;
PROC SORT DATA=visits_6;
BY vis_id DESCENDING i_subch;
RUN;
PROC SORT DATA=visits_6 OUT=visits_7 NODUPKEY;
BY vis_id ;
RUN;

**	there are some visits that have codes that do not belong to any subchapter.
	they are wrong.
	for these visits there was no line with i_subch=1.
	I do not want to count these, so I will delete they label;
DATA visits_8;
SET visits_7;
IF i_subch=0 THEN subchap=' ';
RUN;

** tabilate the subchapters to count the number of visits under each;
PROC FREQ DATA=visits_8;
TABLES subchap /MISSING ;
RUN;

**	There are 1000 visits.
	Cout how many of these are labeled with each subchapter.
	Calculate the proportions that the counts represent out of the total number of visits (1000) 

	ANSWER (subchapters that more than 1% of visits are labeled with):

	E70-E90 , G40-G47 , I30-I52
	L60-L75, L80-L99 , M05-M14 , Q80-Q89
	T80-T88 , Z00-Z13 , Z80-Z99 ;




/* 5 */
*	Within patients, which sub-chapters have a frequency of 1% or more? ;
**	I guess this means that out of the 300 patints, what proportion can
	be classified as having at least one visit in a certain subchapter. ;
** 	For each patient I will keep only one visit per chapter. ;
PROC SORT DATA=visits_8 OUT=visits_9 NODUPKEY;
BY patient_id subchap;
RUN;

** I also need a variable that would tell me how many patients I have;
DATA visits_9;
SET visits_9;
BY patient_id;
IF first.patient_id=1 THEN pat_count+1;
RUN;
**	ANSWER:
	there are 291 patients;

PROC FREQ DATA=visits_8;
TABLES subchap;
RUN;
**	thre are 291 pateints,
	each of them (potentially) labeled with several diagnostics (subchapters) within different visits
	calculate the proportion of patients labeled with each of the subchapters
	select the subchapters that more than 1% of pateints are labeled with ; 

** ANSWER (subchapters that more than 1% of patients had visits for):
	A30-A49 , A50-A64 , A92-A99
	B00-B09 , B15-B19 , B35-B49 , B65-B83 , B95-B98 
	C00-C14 , C30-C39 , C43-C44 , C45-C49 , C51-C58 , C81-C96 
	D10-D36 , D37-D48 , D55-D59 , D80-D89 
	E10-E14 , E20-E35 , E50-E64 , E70-E90 
	F10-F19 , F20-F29 , F30-F39 , F90-F98 
	G20-G26 , G35-G37 , G40-G47 , G50-G59 , G60-G64 , G80-G83 , G90-G99
	H00-H06 , H15-H22 , H30-H36 , H43-H45 , H49-H52 , H65-H75 
	I05-I09 , I20-I25 , I30-I52 , I60-I69 , I70-I79 
	J30-J39 , J90-J94 ,
	K00-K14 , K20-K31 , K40-K46 , K55-K64
	L00-L08 , L10-L14 , L20-L30 , L40-L45 , L55-L59 , L60-L75 , L80-L99 
	M00-M03 , M05-M14 , M20-M25 , M45-M49 , M50-M54 , M65-M68 , M70-M79 , M80-M85 , M86-M90 , M95-M99
	N30-N39	, N40-N51 , N80-N98 ,
	O00-O08 , O20-O29 , O30-O48 , O60-O75 , O80-O84 , O85-O92 , O94-O99
	P35-P39 , P50-P61 , P80-P83
	Q00-Q07 , Q10-Q18 , Q35-Q37, Q38-Q45 , Q65-Q79 , Q80-Q89 Q90-Q99
	R25-R29 , R47-R49 , R50-R69 , R83-R89 , R90-R94
	S00-S09 , S20-S29 , S30-S39 , S40-S49 , S50-S59 , S60-S69 , S70-S79 , S80-S89 , S90-S99
	T00-T07 , T08-T14 , T20-T25 , T29-T32 , T36-T50 , T51-T65 , T80-T88 , T90-T98
	X85-Y09
	Z00-Z13, Z20-Z29 , Z30-Z39 , Z40-Z54 , Z80-Z99
;



/* 6 */
*	How many patients have a code for "Multiple Sclerosis"? ;
*	I did not do this. Should be similar tot what I did before ;

/* 7 */
*	Are there any invalid codes in the dataset? If so, how many? ;
**	there are several visits with codes that start with a number - ICD10 codes do not start with a number ;
**	there are also some visits with missing main diagnosis ;

***	I can come up with a set of rules that make a correct ICD10 code:
	(1) starts with a letter,
	(2) after the letter contains only numbers,
	(3) has a length of 4 characters

	If any of these rules are broken, I consider the ICD10 code invalid ;

DATA visits_9;
SET visits_2;
i_error_1=ANYALPHA(main_dia,1);
i_error_2=ANYALPHA(main_dia,2);
len_main=LENGTH(main_dia);
RUN;

DATA visits_9;
SET visits_9;
i_error=0;
IF i_error_1^=1 OR i_error_2^=0 OR len_main^=4 THEN i_error=1;
RUN;


PROC FREQ DATA=visits_9;
TABLES i_error;
RUN;

/* 93 observatiosn have have some error (or are missing) */
/* 907 observations are correct - they start in a character */

PROC PRINT DATA=visits_9;
VAR patient_id main_dia i_error_1 i_error_2 len_main i_error;
WHERE i_error=1;
RUN;

