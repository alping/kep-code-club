# 181207 – Code-storming (aka brainstorming)

In this month's edition of the code club you'll have the chance to "code-storm"
with your colleagues. The focus will be on the day-to-day issues we all face
when coding and handling data. In the spirit of giving and sharing (it is
getting close to the holidays after all) we thought this code club could be more
about sharing tips and tricks, suggestions, ideas, and of course any problems
you might have gotten stuck on. A code club like this has been suggested by many
of you, and we hope it'll prove useful.

## This code club you have the floor to share: 
- How you have gotten around a particularly pesky problem
- A nifty piece of code you have used
- Ways to make automatic tables
- A cool function you think others would benefit from
- And more! 

Day-to-day coding sometimes presents with small, but equally irritating code
problems that you may have been wondering if there is an easier way to get
around---this is the day you can get a wide variety of opinions on the matter!
Chances are, if you are experiencing a problem, then others either are, or have,
faced the same one.

If you have a larger problem that you feel would be better suited as the topic
of an entire session, please email us to make that happen.

Let's get together and make our coding lives easier, smoother, and more fun!

Merry Coding, and a Happy New Dataset to All! ⛄
