import pandas as pd
from itertools import combinations
from math import factorial

###############################################################################
# Helper function
###############################################################################
flatten = lambda l: [item for sublist in l for item in sublist]

###############################################################################
# Import data
###############################################################################
data = pd.read_csv('../data/disease-patterns.csv', index_col='id')
data.head()
###############################################################################

###############################################################################
# Solution 1 - Using groupby
# Added after the session, inspired by Renatas STATA solution
###############################################################################
data.groupby(data.columns.tolist()).size()

###############################################################################
# Solution 3 - Using groupby
# Added after the session
###############################################################################
combins = flatten([combinations(data.columns, i) for i in range(1, data.shape[1] + 1)])

grp_data = data.groupby(data.columns.tolist()).size()

res_3 = pd.Series({
    ' + '.join(c): grp_data.xs((1, ) * len(c), level=c).sum()
    for c in combins})

res_3['none'] = grp_data.loc[0, 0, 0, 0, 0, 0]

res_3.sort_values(ascending=False)

###############################################################################
# Solution 1.1
###############################################################################
data_1_1 = data.copy()

data_1_1['pattern'] = data_1_1.astype(str).apply(
    lambda x: ''.join(x), axis='columns')

data_1_1['pattern'].value_counts()

###############################################################################
# Solution 1.2
###############################################################################
data_1_2 = data.copy()

data_1_2['pattern'] = (data_1_2 * [100000, 10000, 1000, 100, 10, 1]) \
    .sum(axis='columns').astype(str).str.pad(6, 'left', '0')

data_1_2['pattern'].value_counts()

###############################################################################
# Solution 1.3
###############################################################################
data_1_3 = data.copy()

data_1_3.apply(lambda x: x.mask(x == 1, x.name)).replace(0, '') \
        .sum(axis='columns').value_counts()

###############################################################################
# Solution 2
###############################################################################
n_combins = list(combinations(data.columns, 2))

result_2 = pd.Series(
    {' + '.join(c): data[list(c)].all(axis='columns').sum()
     for c in n_combins})

result_2.sort_values(ascending=False)

###############################################################################
# Solution 3
###############################################################################
all_combins = flatten([combinations(data.columns, i) for i in range(1, data.shape[1] + 1)])

result_3 = pd.Series(
    {' + '.join(c): data[list(c)].all(axis='columns').sum()
     for c in all_combins})

result_3['None'] = (~data.any(axis='columns')).sum()

result_3.sort_values(ascending=False)

###############################################################################
# Number of possible combinations
###############################################################################
def nr_of_combinations(n, k):
    return int(factorial(n) / (factorial(k) * factorial(n - k)))

ncols = data.shape[1]
nr_comb = pd.Series({i: nr_of_combinations(ncols, i) for i in range(ncols + 1)})

nr_comb
nr_comb.sum()
